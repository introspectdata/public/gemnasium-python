package pkgmngr

// ErrNoCompatibleFile is raised when there's no compatible file in the directory.
type ErrNoCompatibleFile struct {
	Path string
}

// Error returns the error message.
func (e ErrNoCompatibleFile) Error() string {
	return "No compatible file in " + e.Path
}
