package pkgmngr

import (
	"os"
	"os/exec"
	"path/filepath"
)

// PackageManager is a wrapper around a CLI tool for fetching & installing dependencies
type PackageManager interface {
	InstallDependencies(opts InstallOptions) error
	DependencyGraph() ([]byte, error)
	DepFile() string
}

// InstallOptions wraps a PackageManager's fetching and installation behaviors
type InstallOptions struct {
	CacheDir  string
	SkipFetch bool
}

// NewPackageManager returns an interface for managing dependencies
func NewPackageManager(projectPath string) (PackageManager, error) {
	// find compatible dependency file
	depFile, err := findFile(projectPath)
	if err != nil {
		return nil, err
	}

	switch depFile {
	case "Pipfile":
		return pipenv{projectPath: projectPath}, nil
	default:
		return pip{
			projectPath: projectPath,
			depFile:     depFile,
		}, nil
	}
}

func setupCmd(path string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr
	return cmd
}

// findFile returns the basename of a compatible file found in given directory.
func findFile(path string) (string, error) {
	for _, filename := range []string{"requirements.txt", "requirements.pip", "Pipfile", "requires.txt"} {
		if _, err := os.Stat(filepath.Join(path, filename)); err == nil {
			return filename, nil
		}
	}
	return "", ErrNoCompatibleFile{path}
}
