# Gemnasium Python analyzer changelog

## v2.2.4
- Rename `PIP_DEPENDENCY_PATH` to `DS_PIP_DEPENDENCY_PATH` (!24)

## v2.2.3
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!21)

## v2.2.2
- Fix dependency list, include dependency files which do not have any vulnerabilities (!14)

## v2.2.1
- Sort the dependency files and their dependencies (!13)

## v2.2.0
- List the dependency files and their dependencies (!12)

## v2.1.0
- Extend the base python image with gcc build environment for pip wheels (@janw)
- Lock to python3.6
- Add `PIP_DEPENDENCY_PATH` EnvVar for relying on local dependencies

## v2.0.2
- Bump common to v2.1.6
- Bump gemnasium to v2.1.2

## v2.0.1
- Bump common to v2.1.5, introduce remediations
- Bump gemnasium to v2.1.1, introduce stable report order
- Fix `Pipfile` support by using `pipenv`

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
