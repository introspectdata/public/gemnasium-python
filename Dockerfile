FROM python:3.6-slim-stretch
ARG PIPENV_VERSION==2018.11.26

RUN pip install pipdeptree pipenv==$PIPENV_VERSION

# Include gcc build env
RUN apt-get update -y && \
    apt-get install --no-install-recommends -y \
    build-essential \
    libffi-dev \
    musl-dev \
    libssl-dev \
    python3-dev && \
    rm -rf /var/lib/apt/lists/*

COPY analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
