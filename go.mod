module gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/v2

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.4.2
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2 v2.2.4
)
